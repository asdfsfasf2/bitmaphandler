#include "BitmapHandler.h"
#include <stdio.h>

unsigned char *LoadBitmapFile(char *filename, BITMAPINFOHEADER *bitmapInfoHeader)
{
	FILE *filePtr;
	BITMAPFILEHEADER bitmapFileHeader;
	unsigned char *bitmapImage;
	int imageIdx = 0;
	unsigned char tempRGB;

	fopen_s(&filePtr, filename, "rb");
	if (filePtr == NULL)
		return NULL;

	fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);

	if (bitmapFileHeader.bfType != 0x4D42)
	{
		fclose(filePtr);
		return NULL;
	}

	fread(bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);

	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	bitmapImage = (unsigned char*)malloc(bitmapInfoHeader->biSizeImage);

	if (!bitmapImage)
	{
		free(bitmapImage);
		fclose(filePtr);
		return NULL;
	}

	fread(bitmapImage, bitmapInfoHeader->biSizeImage, 1, filePtr);

	if (bitmapImage == NULL)
	{
		fclose(filePtr);
		return NULL;
	}

	fclose(filePtr);
	return bitmapImage;
}

void SaveBitMapFile(char *fileName, BITMAPINFOHEADER *bitmapInfoHeader, unsigned char *buffer)
{
	BITMAPFILEHEADER bfh = { 0 };

	bfh.bfType = 'B' + ('M' << 8);
	bfh.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);
	bfh.bfSize = bfh.bfOffBits + bitmapInfoHeader->biSizeImage;

	HANDLE hFile = CreateFile(fileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (!hFile)
	{
		return;
	}

	DWORD dwWritten = 0;
	WriteFile(hFile, &bfh, sizeof(bfh), &dwWritten, NULL);
	WriteFile(hFile, bitmapInfoHeader, sizeof(*bitmapInfoHeader), &dwWritten, NULL);

	char empty[4] = { 0, 0, 0, 0 };

	int Width = bitmapInfoHeader->biWidth * 3;
	int Height = bitmapInfoHeader->biHeight;

	int total = Width * Height;
	int fileSize = total;
	if (Width % 4 != 0)
	{
		Width = Width + (4 - Width % 4);
		//fileSize = Height * (Width + (4 - Width % 4));
	}

	for (int i = 0; i < 74 * 3 + 2; ++i)
		printf("%02x ", buffer[i]);

	//for (int i = 0; i < total; ++i)
	//	WriteFile(hFILE, &buffer[i])

	if (Width % 4 == 0)
	{
		WriteFile(hFile, buffer, bitmapInfoHeader->biSizeImage, &dwWritten, NULL);
	}
	else
	{
		//char* empty = new char[4 - Width % 4];
		for (int i = 0; i < bitmapInfoHeader->biHeight; ++i)
		{
			WriteFile(hFile, &buffer[i * Width], Width, &dwWritten, NULL);
			//WriteFile(hFile, &empty, 4 - Width % 4, &dwWritten, NULL);
		}

		//for (int i = total; i < fileSize; ++i)
		//	WriteFile(hFile, &empty, 4, &dwWritten, NULL);
		//delete empty;
	}

	CloseHandle(hFile);
}

